//
//  ViewController.swift
//  ProblemTouchMovedOnlyOnce
//
//  Created by Maxence Frère on 19/11/2020.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var button: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        button.addTarget(self, action: #selector(buttonTouched), for: .touchDown)
    }

    @objc
    func buttonTouched(button:UIButton) {
        print("Button ACTION!")
    }
}

extension ViewController {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("ViewController: touches began - ")
        super.touchesBegan(touches, with: event)
        print("ViewController: touches began - end\n")
    }

    // FIXME: Only called one in the view controller
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("ViewController: touches moved - ")
        super.touchesMoved(touches, with: event)
        print("ViewController: touches moved - end\n")
    }

    // FIXME: Not called if one touchesMoved has already been triggered
    // works when directly triggered after touchesBegan
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("ViewController: touches ended")
        super.touchesEnded(touches, with: event)
        print("ViewController: touches ended - end\n")
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("ViewController: touches cancelled")
        super.touchesCancelled(touches, with: event)
        print("ViewController: touches cancelled - end\n")
    }
}

extension UIButton {
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Button: touches began - ")
        super.touchesBegan(touches, with: event)
        next?.touchesBegan(touches, with: event)
        if next == nil { print("next was nil!") }
        print("Button: touches began - end\n")
    }

    open override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Button: touches moved - ")
        super.touchesMoved(touches, with: event)
        next?.touchesMoved(touches, with: event)
        if next == nil { print("next was nil!") }
        print("Button: touches moved - end\n")
    }

    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Button: touches ended")
        super.touchesEnded(touches, with: event)
        next?.touchesEnded(touches, with: event)
        if next == nil { print("next was nil!") }
        print("Button: touches ended - end\n")
    }

    open override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Button: touches cancelled")
        super.touchesCancelled(touches, with: event)
        next?.touchesCancelled(touches, with: event)
        if next == nil { print("next was nil!") }
        print("Button: touches cancelled - end\n")
    }
}
